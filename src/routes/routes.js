const { Router } = require('express');
const router = Router();

const { getUsers,getCount,getHealtCheck } = require('../controllers/index.controller');

router.get('/users/:user', getUsers);

router.get('/count', getCount);

router.get('/health', getHealtCheck);

module.exports = router;