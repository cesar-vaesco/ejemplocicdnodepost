const dbConfig = require("../config/db.config.js");
const { Pool } = require('pg');
var os = require("os");


const pool = new Pool({
    host: dbConfig.HOST,
    user: dbConfig.USER,
    password: dbConfig.PASSWORD,
    database: dbConfig.DB,
    port: dbConfig.PORT
});

const getUsers = async (req, res) => {
    console.log(os.hostname());
    const nombre = req.params.user;
    console.log("Nombre ", nombre);
    const resFunction = await pool.query('Select * from usuarios', [nombre]);
    console.log(resFunction.rows);

    res.status(200).json(resFunction.rows);
}

const getCount = async (req, res) => {
    console.log(os.hostname());
    const response = await pool.query("select * from usuarios");
    res.status(200).send(response.rows);
  };

const getHealtCheck = async(req, res) => {
    console.log(os.hostname());
    const response = "health check ok hostname: "+os.hostname();
    res.status(200).send(response);
};  


module.exports = {
    getUsers,
    getCount,
    getHealtCheck
}